#pragma once

#define MAX_DISPLAY_LEN 64

/* If set, SR is enabled */
// #define SR
/* If set, SR logging is enabled */
// LOG_ENABLE

#define PGIE_CLASS_ID_VEHICLE 0
#define PGIE_CLASS_ID_PERSON 2

/* The muxer output resolution must be set if the input streams will be of
 * different resolution. The muxer will scale all the input frames to this
 * resolution. */
#define MUXER_OUTPUT_WIDTH 1920
#define MUXER_OUTPUT_HEIGHT 1080

/* Muxer batch formation timeout, for e.g. 40 millisec. Should ideally be set
 * based on the fastest source's framerate. */
#define MUXER_BATCH_TIMEOUT_USEC 40000

/* SR options */
#define SMART_REC_CONTAINER 0
#define VIDEO_CACHE_SIZE 15
#define SMART_REC_DEFAULT_DURATION (5*60)
#define START_TIME 2

/* start/stop logic */

/* See firmware design document to see interplay between these values */
#define MAX_CONFIDENCE_VALUE         34
#define START_RECORD_CONFIDENCE      15
#define STOP_RECORD_CONFIDENCE       5 
/* Period in us in which current confidence value is reduced */
#define CONFIDENCE_SUBTRATION_PERIOD 200000
